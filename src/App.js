import ToDoList from "./components/toDoList/todolist";

function App() {
  return (
    <div className="App">
      <ToDoList />
    </div>
  );
}

export default App;
