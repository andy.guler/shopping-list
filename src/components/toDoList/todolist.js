import { useState } from "react";
import { useRef } from "react";
import { useEffect } from "react";
import "./todolist.css";

const ToDoList = () => {
  const [item, setItem] = useState();
  const [list, setList] = useState([]);

  const getData = (e) => {
    setItem(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = item;
    setList((item) => [...item, data]);
  };

  const handleDelete = (index) => {
    list.splice(index, 1)
    setItem([list])
  }

  return (
    <div className="to-do-list-container">
      <div className="title">
        <p>Shopping List</p>
        <form className="input">
          <input placeholder="Add your Item" onChange={getData} />
          <button className="btn" onClick={handleSubmit}>
            Add
          </button>
        </form>
      </div>
      <div className="content-container">
        <div className="content">
          <ul className="items">
            {list.map((item, index) => (
              <li key={item + index}>
                {item}
                <button className="delete-btn" onClick={() => handleDelete(index)}>x</button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ToDoList;
